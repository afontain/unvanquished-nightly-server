#
# This files brings together all the parts managed by nix. That's the one
# called by the `nix` commands. It includes dev tools and handles the build of
# the engine and game.
#

{
  ######
  # Part 1: sources
  ######

  inputs = rec {

      nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";

      # tools
      urcheon.url = "github:DaemonEngine/Urcheon";
      urcheon.inputs.nixpkgs.follows = "nixpkgs";

    };


  ######
  # Part 2: outputs
  ######

  outputs = { self, nixpkgs, urcheon }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in {
      packages.x86_64-linux = {

        devShell.x86_64-linux = pkgs.mkShell {
          buildInputs =
            pkgs.lib.attrValues urcheon.packages.x86_64-linux ++
            [ pkgs.git ];
        };

      };
    };
}
