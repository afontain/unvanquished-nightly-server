{ lib, stdenv, git
, sources
, crunch
, iqm
, netradiant
, sloth
, urcheon
, last-release-version  # for partial dpk
, last-release-sources  # for partial dpk
}:

let
  build-dpk = dpk: stdenv.mkDerivation {
    name = "one-unvanquished-dpk";

    unpackPhase = "true"; # no src= attribute
  };

in stdenv.mkDerivation rec {
  name = "unvanquished-assets";

  unpackPhase = "true"; # no src= attribute

  buildInputs = [
    crunch
    iqm
    sloth
    netradiant
    urcheon
    git
  ];

  propagatedBuildInputs = [
  ];

  buildPhase = ''
    export PAKPATH=${last-release-sources}

    cp -r ${sources}/src src
    chmod +w -R assets

    # Note we purposefully ignore unvanquished_src.dpkdir. Note this will break
    # the day one pak that isn't "unvanquished" starts with something that
    # isn't a m (like map-*) or a r (like res-*)
    echo       "## urcheon prepare src/[rm]*.dpkdir"
    ${urcheon}/bin/urcheon prepare src/[rm]*.dpkdir
    echo       "## urcheon build -r ${last-release-version} src/[rm]*.dpkdir"
    ${urcheon}/bin/urcheon build -r ${last-release-version} src/[rm]*.dpkdir
  '';
  installPhase = ''
    echo       "## urcheon package --pak-prefix $out src/[rm]*.dpkdir"
    ${urcheon}/bin/urcheon package --pak-prefix $out src/[rm]*.dpkdir

    # Add (symlinks to) previous release pak
    for oldpak in ${last-release-sources}/pkg/*.dpk; do
      ln -s $oldpak $out/
    done
  '';
}
