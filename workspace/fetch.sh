#!/bin/sh
set -e

###
# This gets the new version of every git repository we depend on on master,
# including every transitive dependency.
#
# The only exception to that is the `iqm` binary, that is locked to a
# known-good svn revision (see the urcheon flake).
#
# Nix will rebuild dependencies as needed to always use the latest version,
# but the assets are managed manually. Should you want a full rebuild, you can
# remove ./UnvaunquishedAssets and launch the process again.
###

# update the git repos handled by nix
nix flake update

# update manually the git repositories for assets
if [ ! -d ~/UnvanquishedAssets ]; then
	git clone https://github.com/UnvanquishedAssets/UnvanquishedAssets/ ~/UnvanquishedAssets
	git -C ~/UnvanquishedAssets submodule update --init --recursive
fi
git -C ~/UnvanquishedAssets submodule foreach git checkout master
git -C ~/UnvanquishedAssets submodule foreach git pull --ff-only
