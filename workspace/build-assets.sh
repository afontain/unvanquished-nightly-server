#!/usr/bin/env bash
set -e

LAST_RELEASE_VERSION="unvanquished/0.54.0"
LAST_RELEASE_PATH="/var/www/cdn.unvanquished.net/unvanquished_0.54.0/pkg"

###
# This build an asset repository in ~/unvanquished-server/pakpath that is to be
# used as pakpath along $LAST_RELEASE_PATH. It uses urcheon to build the
# assets and minimize rebuilds, and nix to build the unvanquished gamelogic.
###


# urcheon may make use of this
export PAKPATH="$LAST_RELEASE_PATH"
# and urcheon WILL make use of this
cd ~/UnvanquishedAssets

urcheon prepare src/*.dpkdir
urcheon build -r $LAST_RELEASE_VERSION src/*.dpkdir
urcheon package src/*.dpkdir

rm -r build/pkg/unvanquished_*

rsync * ~/unvanquished-server/pakpath -rv
