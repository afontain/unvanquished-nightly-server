#!/bin/sh

strip_ascii_color_sequences() {
	# https://stackoverflow.com/questions/17998978/removing-colors-from-output
	sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"
}

echo "Nix part:"
nix flake metadata 2>/dev/null \
	| tail -n+4 \
	| grep -v "follows input" \
	| strip_ascii_color_sequences

echo
echo "Manual part:"
git -C ~/UnvanquishedAssets/ submodule
