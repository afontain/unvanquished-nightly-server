#!/bin/sh
set -e
set -x

./fetch.sh
# nix develop is used to bring the tools (urcheon, iqm, sloth and q3map) into scope without installing them
nix develop -c ./build-assets.sh

./print-all-versions.sh > ../pakpath/current-versions.txt
