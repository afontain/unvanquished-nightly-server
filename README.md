# Unvanquished Nightly Server



## Install

tl;dr: install `nix` (doesn't have to be nixos, you can use nix on any distro), then,

```sh
cd ~
git clone [this repo] ~/unvanquished-server
cd ~/unvanquished-server/workspace
# fetch and build everything
./update.sh
# prepare the systemd config
ln -s ~/unvanquished-server/unvanquished-nightly-server.service ~/.config/systemd/user/
ln -s ~/unvanquished-server/update-unvanquished-nightly-server.service ~/.config/systemd/user/
ln -s ~/unvanquished-server/update-unvanquished-nightly-server.timer ~/.config/systemd/user/
systemctl --user daemon-reload
# enable the systemd units
systemctl --user enable unvanquished-nightly-server.service --now
systemctl --user enable update-unvanquished-nightly-server.timer --now
```

You will also need `git`, `rsync`, `bash` and `flock`.

## Paths

For now, paths are pretty much hardcoded by the shell scripts and systemd
units. Those are (with their subdirectories):

```
~/unvanquished-server for this repo
~/unvanquished-server/workdir for the build tools and the links to the results
~/unvanquished-server/pakpath for the built assets
~/unvanquished-server/homepath for the daemon home directory (instead of ~/.local/share/unvanquished)
~/UnvanquishedAssets for the assets sources (read and written by urcheon)
```
